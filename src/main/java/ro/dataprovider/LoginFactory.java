package ro.dataprovider;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;


import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public final class LoginFactory {

    private static LoginFactory inst;
    public static WebDriver driver;
    public static EventFiringWebDriver iEventFiringWebDriver;
    public static WebEventListener iWebEventListener;
    private ConfigFileReader configFileReader;

    private LoginFactory() {
        driver = new BrowserDriverFactory ().getDriver ();
    }

    public static LoginFactory getInstanceOfLoginFactory() {
        if (inst == null) {
            inst = new LoginFactory ();
        }
        return inst;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void initListener(){
        iEventFiringWebDriver = new EventFiringWebDriver (driver);
        // Create object of EventListerHandler to register it with EventFiringWebDriver
        iWebEventListener = new WebEventListener();
        iEventFiringWebDriver.register(iWebEventListener);
        driver = iEventFiringWebDriver;
    }

    public void openMaximizeWindow(ConfigFileReader iConfigFileReader) {

        driver.manage ().window ().maximize ();
        driver.manage().deleteAllCookies();
        driver.manage ().timeouts ().implicitlyWait ( iConfigFileReader.getImplicitlyWait (), TimeUnit.SECONDS );
        driver.manage().timeouts().implicitlyWait(iConfigFileReader.getImplicitlyWait (), TimeUnit.SECONDS);

        driver.navigate ().to ( iConfigFileReader.getApplicationUrl () );
        System.out.println ( "URL is entered. The website is maximized and opened." );
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement ( by );
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }



}

//    public void setInvalidUsername(String username, WebElement emailElement) {
//        emailElement.sendKeys ( username );
//    }

//    public void setInvalidPassword(String password, WebElement passwordElement) {
//        passwd.sendKeys(password);
//    }

//    public void checkLogo(By logoBy) {
//        WebElement image = driver.findElement(logoBy);
//
//        if (image.isDisplayed() == true) {
//            System.out.println("Logo image is present on page.");
//        } else {
//            System.out.println("Logo image is not present on page.");
//        }
//    }

//    public void clickLogOut(By logOutBy) {
//        WebElement logOut = driver.findElement(logOutBy);
//        logOut.click();
//    }



