/*
package tests;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ro.dataprovider.ConfigFileReader;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

import java.io.File;
import java.io.IOException;

@Test(groups = {"AllAddToCartTests"})
public class ModelTest {

    LoginFactory iLoginFactory;
    LoginPage iLoginPage;
    SearchProduct iSearchProduct;
    AddToCart iAddToCart;

    public ModelTest() {
        iLoginFactory = LoginFactory.getInstanceOfLoginFactory();
        iLoginPage = PageFactory.initElements(iLoginFactory.getDriver(), LoginPage.class);
        iSearchProduct = PageFactory.initElements(iLoginFactory.getDriver(), SearchProduct.class);
        iAddToCart = PageFactory.initElements(iLoginFactory.getDriver(), AddToCart.class);
    }

    @BeforeTest
    public void validLoginTest() {
        ConfigFileReader iConfigFileReader = new ConfigFileReader ();

        iLoginFactory.initListener ();
        iLoginFactory.openMaximizeWindow(iConfigFileReader);

        String iUsername = iConfigFileReader.getUsername();
        iLoginFactory.setValidUsername ( iUsername, iLoginPage.emailWebElement);
        iLoginPage.continuaWebElement.click ();

        String iPassword = iConfigFileReader.getPassword();
        iLoginFactory.setValidPassword ( iPassword, iLoginPage.passwordWebElement);
        iLoginPage.continuaLoginWebElement.click ();

        System.out.println ( "User is successfully logged in." );
    }

    @Test(groups = {"smoke"})
    @Description(name = "Add to card", description = "Add a product to cart, go to cart, select a warranty, delete the product from cart.", priority = 1, disabled = false, groups = {"smoke"})
    public void addToCart() {

        SearchProduct iSearchProduct = PageFactory.initElements ( iLoginFactory.getDriver (), SearchProduct.class );
        AddToCart iAddToCart = PageFactory.initElements ( iLoginFactory.getDriver (), AddToCart.class );

        iSearchProduct.searchBoxWebElement.sendKeys ( "Monitor Dell" );
        System.out.println ( "Search by specific and valid keywords." );

        iSearchProduct.searchIconWebElement.click ();
        System.out.println ( "Click search." );

        iSearchProduct.searchResultWebElement.click ();

        iAddToCart.adaugaInCosWebElement.click ();

        iAddToCart.assertTextProdusCos ();

        iAddToCart.veziDetaliiCosWebElement.click ();
        System.out.println ( "Click on: 'Vezi detalii cos'" );

        iAddToCart.chboxGarantiaPlusWebElement.click ();
        System.out.println ( "Select checkbox 'Garantia Plus 2 ANI'." );

        iAddToCart.stergeProdusCosWebElement.click ();
        System.out.println ( "The product was deleted from cart." );

        iAddToCart.assertTextCosGol ();

        iAddToCart.buttonBackMagazinWebElement.click ();
        System.out.println ( "Click on 'Intoarce-te la magazin'." );

        iLoginFactory.getDriver ().quit ();
    }
    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            System.out.println(testResult.getStatus());
            File scrFile = ((TakesScreenshot)iLoginFactory.getDriver ()).getScreenshotAs( OutputType.FILE);
            //FileUtils.copyFile(scrFile, new File("C:\\Users\\a.pila\\Documents\\workspace-sts\\emag-project\\src\\main\\resources\\screenshots\\" + testResult.getName() +  ".jpg"));
            FileUtils.copyFile(scrFile, iLoginFactory. + testResult.getName() +  ".jpg"));

        }
    }
}*/
